class Problem02
	
	def initialize(threshold)
		@threshold = threshold
	end
	
	def run
		penultimate = fib = sum = 0
		last = 1
		while fib < @threshold do
			sum += fib if fib % 2 == 0
			fib = penultimate + last
			penultimate = last
			last = fib
		end
		
		puts "result: #{sum}"
	end
end

Problem02.new(ARGV[0].to_i).run
