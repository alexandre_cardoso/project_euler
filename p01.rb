class Problem01
	
	def initialize(threshold)
		@threshold = threshold
	end
	
	def run
		sum = 0
		for x in 1...@threshold do
			sum += x if (x % 3 == 0 || x % 5 == 0)
		end
		puts "result: #{sum}"
	end
end

Problem01.new(1000).run
